grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"

grails.project.dependency.resolution = {

    // inherit Grails' default dependencies
    inherits("global") {
    }

    log "warn" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    repositories {
        grailsCentral()
        mavenCentral()
    }

    dependencies {

        runtime 'com.github.groovy-wslite:groovy-wslite:0.7.2'
        runtime 'net.jodah:expiringmap:0.5.0'

        // Add seal libraries
        runtime('dk.sosi.seal:seal:2.1.3') {
            exclude "axis-jaxrpc"
        }
    }

    plugins {
        compile ':spring-security-core:2.0-RC4'
        build(":tomcat:7.0.54",
              ":release:3.0.1",
              ":rest-client-builder:2.0.3") {
            export = false
        }
    }
}

def corePluginDirectory = '../opentele-server-core-plugin'
grails.plugin.location.'OpenteleServerCorePlugin' = corePluginDirectory
