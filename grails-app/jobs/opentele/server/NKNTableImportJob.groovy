package opentele.server

class NKNTableImportJob {

    def importNKNTableService
    def grailsApplication
    def concurrent = false

    static triggers = {
        cron(name: 'WeeklyNKNTableImportJob', // cronExpression: "0 0/2 * * * ?") // Every 2 minutes
                cronExpression: "0 0 23 * * ?") // Every night at 23:00
    }

    def execute() {
        log.debug("Importing NKN Table")
        importNKNTableService.importNKNTable()
    }

}
