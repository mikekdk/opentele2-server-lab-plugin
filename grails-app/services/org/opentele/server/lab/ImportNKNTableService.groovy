package org.opentele.server.lab

import groovy.util.slurpersupport.GPathResult
import org.opentele.server.lab.pogo.AnalysisDetails
import org.springframework.beans.factory.annotation.Value

class ImportNKNTableService {

    @Value('${lab.nknTableUrl:""}')
    String nknTableUrl

    @Value('${lab.npuCodes:""}')
    String npuCodesString

    @Value('${lab.nknTablePropertiesPath:""}')
    String nknTablePropertiesPath

    def grailsApplication
    List npuCodes

    def importNKNTable() {
        npuCodes = npuCodesString.split(',')

        String cleanedNKNTableText = fetchXmlText()
        GPathResult xml = new XmlSlurper().parseText(cleanedNKNTableText)
        def analysisDetailsNodes = extractAnalysisDetailsNodes(xml)
        List<AnalysisDetails> analysisDetailsList = extractAnalysisDetails(analysisDetailsNodes, npuCodes)
        writeToPropertiesFile(analysisDetailsList)
    }

    private String fetchXmlText() {
        URL nknTableUrl = new URL(nknTableUrl)
        String nknTableText = nknTableUrl.text
        String cleanedNKNTableText = nknTableText.substring(nknTableText.indexOf('<')) // remove byte order mark if its there
        cleanedNKNTableText
    }

    private static def extractAnalysisDetailsNodes(GPathResult xmlResponse) {

        if (findChildrenWithName(xmlResponse, 'LaboratoryAnalysisFile').size() == 0) {
            throw new Exception("lab.errors.output.missingLaboratoryAnalysisFile")
        }
        def laboratoryAnalysisFile = xmlResponse.LaboratoryAnalysisFile

        if (findChildrenWithName(laboratoryAnalysisFile, 'AnalysisDetails').size() == 0) {
            throw new Exception("lab.errors.output.missingAnalysisDetails")
        }
        laboratoryAnalysisFile.AnalysisDetails
    }

    private static def findChildrenWithName(parent, String childName) {
        parent.children().findAll({ it.name() == childName })
    }

    private static List<AnalysisDetails> extractAnalysisDetails(analysisDetailsNodes, List<String> npuCodes) {
        List<AnalysisDetails> analysisDetailsList = []
        analysisDetailsNodes.each { analysisDetailsNode ->
            String code = analysisDetailsNode.Code.text()

            if (!(code in npuCodes)) {
                return
            }
            String fullNameText = analysisDetailsNode.FullName.text()
            def (String fullName, String unit) = extractFullNameAndUnit(fullNameText)
            String shortName = analysisDetailsNode.ShortName.text()

            AnalysisDetails analysisDetails = new AnalysisDetails(code: code,
                    fullName: fullName, shortName: shortName, unit: unit)
            analysisDetailsList += analysisDetails
        }
        analysisDetailsList
    }

    private static List extractFullNameAndUnit(String fullNameText) {
        String fullName = fullNameText
        String unit = ""
        String separator = " = ? "
        int indexOfSeparator = fullName.indexOf(separator)
        if (indexOfSeparator > -1) {
            unit = fullNameText.substring(indexOfSeparator + separator.length())
            fullName = fullNameText.substring(0, indexOfSeparator)
        }
        [fullName, unit]
    }

    private void writeToPropertiesFile(List<AnalysisDetails> analysisDetailsList) {
        Properties properties = new Properties()
        File propertiesFile = new File(nknTablePropertiesPath)
        if (propertiesFile.exists()) {
            properties.load(propertiesFile.newDataInputStream())
        }

        analysisDetailsList.each { AnalysisDetails analysisDetails ->
            String code = analysisDetails.code
            String shortName = analysisDetails.shortName
            String fullName = analysisDetails.fullName
            String unit = analysisDetails.unit

            if (shortName) {
                properties.setProperty(code + ".shortName", shortName)
            }
            if (fullName) {
                properties.setProperty(code + ".fullName", fullName)
            }
            if (unit) {
                properties.setProperty(code + ".unit", unit)
            }
        }

        properties.store(propertiesFile.newWriter(), null)
    }

}
