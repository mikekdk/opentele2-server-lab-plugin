package org.opentele.server.lab.measurements

public enum Indicator {

    // --*-- Enums --*--

    Below('\u2193'), // down arrow
    Unknown('\u2195'), // up down arrow
    Above('\u2191'), // up arrow
    None('')

    // --*-- Fields --*--

    String unicodeCharacter

    // --*-- Constructors --*--

    Indicator(String unicodeCharacter) {
        this.unicodeCharacter = unicodeCharacter
    }

}