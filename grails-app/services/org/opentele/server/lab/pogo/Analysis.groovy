package org.opentele.server.lab.pogo

import groovy.transform.ToString

@ToString
class Analysis {

    def analysisCode
    def analysisCodeType
    def analysisCodeResponsible
    def analysisShortName
    def analysisCompleteName
    def order

    RequisitionGroup requisitionGroup
}
