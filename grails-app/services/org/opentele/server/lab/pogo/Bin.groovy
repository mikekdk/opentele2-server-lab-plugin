package org.opentele.server.lab.pogo

import groovy.transform.ToString

@ToString
class Bin {

    def objectIdentifier
    def objectCode
    def objectExtensionCode
    def originalObjectSize

}
