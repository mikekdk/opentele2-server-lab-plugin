package org.opentele.server.lab.pogo

import groovy.transform.ToString

@ToString
class CCReceiver {

    def identifier
    def identifierCodeOrLocal

    def organisationName
    def departmentName
    def unitName
}
