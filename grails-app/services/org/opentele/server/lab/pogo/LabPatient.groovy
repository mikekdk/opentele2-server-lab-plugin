package org.opentele.server.lab.pogo

import groovy.transform.ToString

@ToString
class LabPatient {

    def cpr // Kan være erstatn. cpr

    def givenName
    def surname

    def consentGiven // true el. false.

}
