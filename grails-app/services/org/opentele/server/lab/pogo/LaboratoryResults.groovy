package org.opentele.server.lab.pogo

import groovy.transform.ToString

@ToString
class LaboratoryResults {

    GeneralResultInformation generalResultInformation = new GeneralResultInformation()
    List<Result> results = new ArrayList<Result>()

}
