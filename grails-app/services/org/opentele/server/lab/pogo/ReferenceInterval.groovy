package org.opentele.server.lab.pogo

import groovy.transform.ToString

@ToString
class ReferenceInterval {

    def typeOfInterval
    def lowerLimit
    def upperLimit
    def intervalText
}
