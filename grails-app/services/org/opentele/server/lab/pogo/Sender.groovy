package org.opentele.server.lab.pogo

import groovy.transform.ToString

@ToString
class Sender {
    def eanIdentifier
	def identifier
    def identifierCodeOrLocal
	def organisationName
    def departmentName
    def unitName
	def medicalSpecialityCode
    def fromLabIdentifier

}
